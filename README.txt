
Newsletter Checkbox for Drupal 5.0.x

By Alexis Bellido <alexis@ventanazul.com>

Description
-----------

Newsletter Checkbox adds a checkbox to your contact form and allows your visitors to subscribe to a newsletter.

You can use any newsletter management system by changing the _newsletter_checkbox_form_submit function. The module currently uses Campaign Monitor.

You need to manually change $APIKey and $ListID to work with your Campaign Monitor account.

The module uses NuSoap, included, to send messages to Campaign Monitor API.

If you use this module and find it useful please let me know by visiting:

In English:
http://www.ventanazul.com/webzine/articles/new-subscribers-newsletter-using-drupal

En espa�ol:
http://www.ventanazul.com/articulos/suscripcion-a-boletin-desde-formulario-contacto-drupal

or writing to alexis@ventanazul.com.

I'm also available for paid Drupal work, so let's have a chat, I'll be glad
to help.

To do list
----------

* Settings page.
* Support for other newsletter management systems.
